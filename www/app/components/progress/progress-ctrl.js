(function(){
	'use strict';
	angular.module('aam-roving-rhino')
		.controller('ProgressCtrl', ['gameProgress', ProgressCtrl])
		.directive('rhinoProgress', RhinoProgressDirective);

	function ProgressCtrl(gameProgress){
		var vm = this;
		vm.gameProgress = gameProgress;
		if(vm.gameProgress.getPercentComplete() === 0){
			vm.roundedProgress = 20;
		}
		else{
			vm.roundedProgress = 20*(Math.ceil(Math.abs(gameProgress.getPercentComplete()/20)));		
		}
	}
	
	function RhinoProgressDirective() {
		return {
			restrict: 'E',
			templateUrl: "app/components/progress/rhino-progress.html",
			scope: {
				progress: '='
			},
			link: function (scope, element, attrs) {
				var cover = element.find('#cover')[0];
				cover.style.height = (100 - scope.progress) + '%';
			}
		};
	}

})();