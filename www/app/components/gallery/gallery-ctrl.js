(function() {
	'use strict';

	angular.module('aam-roving-rhino').controller('GalleryCtrl', ['$scope', 'NavigationService', 'galleries', 'GameService', 'BeaconService', '$localstorage', '$ionicPopup', '$rootScope', '$location', '$ionicScrollDelegate', '$cordovaToast', GalleryCtrl]);

	function GalleryCtrl($scope, NavigationService, galleries, GameService, BeaconService, $localstorage, $ionicPopup, $rootScope, $location, $ionicScrollDelegate, $cordovaToast) {
		var vm = this;
		vm.getPoints = GameService.getPoints;
		vm.galleries = galleries;
		var optOutBluetooth = false;
		BeaconService.initialize();
		beaconInit();
		var currentGallery;
		var tasks = getTasks();
		var rssi = { "greatest": -1, "timestamp": new Date() };

		function getTasks() {
			var tasks = [];
			galleries.forEach(function(gallery) {
				(gallery.tasks).forEach(function(task) {
					tasks.push(task);
				});
			});
			console.log(JSON.stringify(tasks));
			return tasks;
		}

		vm.toggleGallery = function(gallery) {
			console.log("Gallery: " + gallery.Name);
			vm.shownGallery = vm.isGalleryShown(gallery) ? null : gallery;
			$ionicScrollDelegate.resize();
			$location.hash(gallery.ShortName + "-task-list");
			$ionicScrollDelegate.anchorScroll(true);
		};

		function beaconInit() {
			BeaconService.checkBluetooth().then(function(isEnabled) {
				if (isEnabled) {
					startRanging(tasks);
				} else if (!optOutBluetooth) {
					showConfirmBluetooth();
				}
			});
		};

		function startRanging(tasks) {
			var regions = tasks.map(function(task) {
				return {
					uuid: task.BeaconUUID,
					major: task.BeaconMajor,
					minor: task.BeaconMinor,
					identifier: task.BeaconIdentifier,
					typeName: "BeaconRegion"
				}
			}).filter(function(region) {
				return region.uuid && region.major;
			});

			regions.forEach(function(region) {
				BeaconService.startRanging(region);
			});
		}

		$rootScope.$on("updated ranging event", function(event, data) {
			var currentTask = tasks.filter(function(task) {
				return task.BeaconMinor == data;
			});
			var currentRssi = Number($localstorage.getObject(data).rssi);
			if (currentTask.length > 0 && (!currentGallery || currentGallery !== currentTask[0].Gallery)) {
				if ((currentRssi != 0) && ((currentRssi > rssi.greatest) || (new Date() - rssi.timestamp > 15000))) {
					var temp = rssi.greatest;
					rssi.greatest = currentRssi;
					rssi.timestamp = new Date();
					currentGallery = currentTask[0].Gallery;
					var message = "You have entered the " + currentGallery + " gallery";
					showEnterGalleryToast(message, 'long', 'bottom');	
				}
				/* took this out because it's annoying. many new gallery
				notifications can stack before the app is able to detect
				if, what gallery task accordions are open. can possibly
				reimplement this if galleries are far enough apart.*/

				/*var allInactive = true;
				for (var gal in vm.galleries) {
					if (vm.isGalleryShown(gal)) {
						allInactive = false;
					}
				}
				if (allInactive) {
					var g = vm.galleries.filter(function(gallery) {
						return gallery.Name === currentTask[0].Gallery;
					});
					if (g) {
						vm.toggleGallery(g[0]);
					}
				}*/
			}
		});

		function showEnterGalleryToast(message, duration, location) {
			$cordovaToast.show(message, duration, location).then(function(success) {
				console.log("Shown Toast");
			}, function(error) {
				console.log("Not shown toast: " + error);
			});
		}

		function showConfirmBluetooth() {
			$ionicPopup.confirm({
				title: 'Bluetooth is not enabled.',
				template: 'Choose Enable to activate Bluetooth.',
				okText: 'Enable Bluetooth',
				cancelText: 'Opt-Out'
			}).then(function(enableBluetooth) {
				if (enableBluetooth) {
					BeaconService.enableBluetooth();
				} else {
					optOutBluetooth = true;
				}
			});
		};

		vm.goToTask = function(galleryName, task) {
			NavigationService.goToTask(galleryName, task);
		};

		vm.isGalleryShown = function(gallery) {
			return vm.shownGallery === gallery;
		};

		vm.isInactive = function(gallery) {
			if (vm.shownGallery && vm.shownGallery !== gallery) {
				return true;
			} else {
				return false;
			}
		};
	};
})();