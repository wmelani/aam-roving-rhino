(function () {
	'use strict';
	angular.module('aam-roving-rhino').controller('VictoryCtrl', ['gameProgress', 'GameService', 'NavigationService', 'SocialMediaService', '$translate', '$ionicHistory', VictoryCtrl]);

	function VictoryCtrl(gameProgress, GameService, NavigationService, SocialMediaService, $translate, $ionicHistory) {
		var vm = this;
		vm.gameProgress = gameProgress;

		vm.restartGame = function () {
			$ionicHistory.clearCache();
			GameService.initializeGame();
			NavigationService.goToGallery();
		};
		
		vm.share = function() {
			//We may want to convert these translations to utilize promises in the future - Troy 6/23
			var translatedTitle = $translate.instant(vm.gameProgress.getTitle());
			SocialMediaService.shareAnywhere({
				message:$translate.instant('SHARE_SCORE_MESSAGE', {title:translatedTitle, score:vm.gameProgress.getPoints()}),
				subject:$translate.instant('SHARE_SCORE_SUBJECT'),
				link: "http://www.asianart.org"
			});
		};
	};

})();