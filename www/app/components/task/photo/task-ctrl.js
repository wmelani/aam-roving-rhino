(function () {
	'use strict';
	angular.module('aam-roving-rhino')
		.controller('PictureTaskCtrl', ['task', 'gallery', '$state', 'NavigationService', 'GameService', '$cordovaCapture', 'Camera', PictureTaskCtrl]);

	function PictureTaskCtrl(task, gallery, $state, NavigationService, GameService, $cordovaCapture, Camera) {
		var vm = this;
		vm.task = task;
		vm.gallery = gallery;

		function onCaptureSuccess(imageData) {
			// Success! Image data is here
			console.log("Image data:" + JSON.stringify(imageData));
			NavigationService.goToPhotoManipulation($state.params.galleryId, $state.params.taskId, imageData);
			vm.disableTakePicture = false;
		};

		function onCaptureFail(err) {
			console.log("Error during camera activity:", JSON.stringify(err));
			vm.disableTakePicture = false;
		};

		vm.takePhoto = function () {
			vm.disableTakePicture = true;
			Camera.takePicture().then(onCaptureSuccess, onCaptureFail);
		};
	}

})();