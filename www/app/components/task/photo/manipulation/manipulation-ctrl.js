(function () {
	'use strict';
	angular.module('aam-roving-rhino')
		.controller('PictureTaskManipulationCtrl', ['$scope', 'task', 'gallery', 'ConfigService', '$localstorage', '$state', 'NavigationService', 'GameService', 'Camera', 'SocialMediaService', '$translate', PictureTaskManipulationCtrl])
		.directive('overlayCanvas', OverlayCanvasDirective);

	function PictureTaskManipulationCtrl($scope, task, gallery, ConfigService, $localStorage, $state, NavigationService, GameService, Camera, SocialMediaService, $translate) {
		var vm = this;
		vm.task = task;
		vm.gallery = gallery;

		$scope.config = vm.config = {};
		$scope.model = vm.model = {};
		ConfigService.getConfig().then(function (conf) {
			vm.imageData = $localStorage.getObject(conf.imageKey);
			console.log("Conf:" + JSON.stringify(conf));
			console.log("Image data:" + JSON.stringify(vm.imageData));
		});

		vm.submit = function (image) {
			//save image to phone
			console.log("Calling save picture...");
			Camera.savePicture(image);
			
			//Mark task complete
			task.IsComplete = true;
			GameService.completeTask(task);

			NavigationService.goToComplete($state.params.galleryId, $state.params.taskId);
		};
		
		function createPost(image){
			return {
				message: $translate.instant('SHARE_IMAGE_MESSAGE'),
				subject: $translate.instant('SHARE_IMAGE_SUBJECT'),
				link: "http://www.asianarts.org",
				image: image
			};
		}
		
		vm.share = function(image) {
			SocialMediaService.shareAnywhere(createPost(image));
		};
		
		vm.shareToInstagram = function(image){
			SocialMediaService.shareToInstagram(createPost(image));
		};
	}

	function OverlayCanvasDirective() {
		return {
			restrict: 'E',
			templateUrl: "app/components/task/photo/manipulation/overlay.html",
			scope: {
				userimgurl: '=',
				overlayimgurl: '=',
				submit: '=',
				share: '=',
				shareToInstagram: '='
			},
			link: function (scope, element, attrs) {
				scope.imageReady = false;
				
				var width = $(window).width() - 20;
				$('.frame').css('height', width);
				$('.frame').css('width', width);

				var $pz = $('#selfie').panzoom({
					minScale: 0.1
				});

				var canvas = document.getElementById('canvas');
				canvas.width = width;
				canvas.height = width;

				var context = canvas.getContext('2d');

				var bg = new Image();
				bg.src = scope.overlayimgurl;
				bg.onload = function () {
					context.drawImage(bg, 0, 0, bg.width, bg.height,
						0, 0, canvas.width, canvas.height)
				};

				scope.samuraize = function () {
					var matrix = $pz.panzoom("getMatrix");
					var scale = matrix[0];
					var offsetLeft = (1 - scale) * $pz.width() / 2;
					var offsetTop = (1 - scale) * $pz.height() / 2;
					var fromLeft = parseFloat(matrix[4]);
					var fromTop = parseFloat(matrix[5]);
					
					var selfie = new Image();
					selfie.src = $pz[0].src;

					context.drawImage(selfie,
						fromLeft + offsetLeft,
						fromTop + offsetTop,
						scale * $pz.width(),
						scale * $pz.height());
					context.drawImage(bg, 0, 0, canvas.width, canvas.height);
					$pz.hide();
					scope.imageReady = true;
				};

				scope.reset = function () {
					context.clearRect(0, 0, canvas.width, canvas.height);
					context.drawImage(bg, 0, 0, canvas.width, canvas.height);
					$pz.show();
					scope.imageReady = false;
				};

				scope.submitProxy = function () {
					try{
						console.log("Getting image data from canvas...");
						scope.submit(canvas.toDataURL().replace(/data:image\/png;base64,/,'')); //Throws an exception when running in browser
					}
					catch(err){
						console.log("Could not get image data from canvas: ", err);
						scope.submit();
					}
				};
				
				scope.shareProxy = function () {
					try{
						console.log("Getting image data from canvas to share...");
						scope.share(canvas.toDataURL()); //Throws an exception when running in browser
					}
					catch(err){
						console.log("Could not get image data from canvas for sharing: ", err);
					}
				};
				
				scope.shareToInstagramProxy = function () {
					try{
						console.log("Getting image data from canvas to share to instagram...");
						scope.shareToInstagram(canvas.toDataURL()); //Throws an exception when running in browser
					}
					catch(err){
						console.log("Could not get image data from canvas for sharing to instagram: ", err);
					}
				};
			}
		};
	};

})();