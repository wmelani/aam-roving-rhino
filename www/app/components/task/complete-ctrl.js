(function () {
	'use strict';
	angular.module('aam-roving-rhino')
		.controller('TaskCompleteCtrl', ['task', '$state', '$scope', 'NavigationService', 'gameProgress', '$translate', TaskCompleteCtrl]);

	function TaskCompleteCtrl(task, $state, $scope, NavigationService, gameProgress, $translate) {
		var vm = this;
		vm.task = task;

		//Set the 'Making Progress' message according to the user's current level.
		var nextTitle = $translate.instant(gameProgress.getNextTitle());
		if (nextTitle) {
			vm.makingProgressMessage = $translate.instant('MAKING_PROGRESS_TOWARD_LEVEL', { title: nextTitle });
		}
		else {
			vm.makingProgressMessage = $translate.instant('MAKING_PROGRESS');
		}
		
		vm.continue = function() {
			//Sometimes we want to go to a progress screen
			if(gameProgress.checkShowProgress()) {
				NavigationService.goToProgress();
			}
			else {
				NavigationService.goToGallery();
			}
		};

		vm.goBackToTask = function () {
			//Page to navigate to depends on what type the task is
			//TODO: this should use the actual NavigationService back method
			NavigationService.goToTask($state.params.galleryId, vm.task);
		};

		$scope.getPoints = gameProgress.getPoints();
	}

})();