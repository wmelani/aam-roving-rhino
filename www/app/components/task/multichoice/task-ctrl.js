(function(){
	'use strict';
	angular.module('aam-roving-rhino')
		.controller('MultipleChoiceTaskCtrl', ['task', 'gallery', '$state', 'NavigationService', 'GameService', MultipleChoiceTaskCtrl]);

	function MultipleChoiceTaskCtrl(task, gallery, $state, NavigationService, GameService){
		var vm = this;
		vm.task = task;
		vm.gallery = gallery;
		
		vm.submit = function(guess) {
			if(guess.IsCorrect) {
				task.IsComplete = true;
				
				GameService.completeTask(task);
			}
			else {
				task.IsComplete = false;
			}
			NavigationService.goToComplete($state.params.galleryId,$state.params.taskId);
		};
	}

})();