(function() {
	'use strict';
	angular.module('aam-roving-rhino')
		.controller('FindTaskCtrl', ['$scope', 'task', 'gallery', '$state', '$localstorage', 'NavigationService', 'GameService', 'BeaconService', '$window', '$rootScope', '$cordovaToast', FindTaskCtrl]);

	function FindTaskCtrl($scope, task, gallery, $state, $localstorage, NavigationService, GameService, BeaconService, $rootScope, $window, $cordovaToast) {
		var vm = this;
		vm.task = task;
		vm.gallery = gallery;
		var region = getRegionFromTask(task);
		taskBeaconInit();

		function taskBeaconInit() {
			BeaconService.checkBluetooth().then(function(isEnabled) {
				if (isEnabled) {
					if (region && region.minor) {
						BeaconService.startRanging(region);
					}
				} else {
					showNoBluetoothToast("Please enable Bluetooth to complete this task.", 'long', 'center');
				}
			});
		};

		vm.submit = function() {
			if (getIsTaskComplete(region)) {
				task.IsComplete = true;
				GameService.completeTask(task);
			} else {
				task.IsComplete = false;
			}
			NavigationService.goToComplete($state.params.galleryId, $state.params.taskId);
		};

		function getRegionFromTask(task) {
			return {
				uuid: task.BeaconUUID,
				major: task.BeaconMajor,
				minor: task.BeaconMinor,
				identifier: task.BeaconIdentifier,
				typeName: "BeaconRegion"
			}
		}

		function getIsTaskComplete(region) {
			if ($localstorage.getObject(region.minor)) {
				var foundBeacon = $localstorage.getObject(region.minor);
				if (foundBeacon && BeaconService.isRecentBeacon(foundBeacon.timestamp) && (foundBeacon.rssi > -75)) {
					console.log("YAY");
					return true;
				} else {
					return false;
				}
			} else {
				console.log("No beacons found");
				return false;
			}
		}

		function showNoBluetoothToast(message, duration, location) {
			$cordovaToast.show(message, duration, location).then(function(success) {
				console.log("Shown Beacon Toast");
			}, function(error) {
				console.log("Not shown toast: " + error);
			});
		}
	};
})();