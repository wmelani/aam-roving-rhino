(function () {
	'use strict';
	angular.module('aam-roving-rhino').controller('WelcomeCtrl', ['$state', 'NavigationService', 'GameService', WelcomeCtrl]);

	function WelcomeCtrl($state, NavigationService, GameService) {
		var vm = this;
		vm.text = "Welcome to this app!";

		if (Number(GameService.getPoints())) {
			vm.continue = true;
		}
		
		vm.continueGame = function(){
			GameService.continueGame();
			NavigationService.goToGallery();
		};

		vm.restartGame = function () {
			GameService.initializeGame();
			NavigationService.goToGallery();
		};
	};

})();