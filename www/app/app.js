// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('aam-roving-rhino', ['ionic', 'local-storage','ngCordova','panzoom','panzoomwidget', 'pascalprecht.translate'])

  .run(function ($ionicPlatform) {
  $ionicPlatform.ready(function () {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }


  });
})
.config(function ($compileProvider,$stateProvider, $urlRouterProvider,$ionicConfigProvider, $translateProvider) {
    $ionicConfigProvider.views.swipeBackEnabled(false);
    $ionicConfigProvider.backButton.previousTitleText(false);
    
    $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel|cdvfile):/);
    
    $translateProvider.useStaticFilesLoader({
      prefix: 'app/config/i18n/',
      suffix: '.json'
    });
    $translateProvider.preferredLanguage('en');
    $translateProvider.useSanitizeValueStrategy('sanitize');

    $stateProvider
      .state('home', {
        url: "/home",
        abstract: true,
        templateUrl: "app/components/home/home.html"
      })
      .state('home.welcome', {
        url: '/welcome',
        views: {
          'home-welcome': {
            templateUrl: 'app/components/welcome/welcome.html',
            controller: 'WelcomeCtrl as vm'
          }
        }
      })
      .state('home.victory', {
        url: '/victory',
        views: {
          'home-victory': {
            templateUrl: 'app/components/victory/victory.html',
            controller: 'VictoryCtrl as vm',
            resolve:{
              gameProgress: function(GameService){
                return GameService.getProgress();
              }
            }
          }
        }
      })
      .state('galleries', {
        url: '/galleries',
        resolve: {
          galleries: function (GalleryService) {
            return GalleryService.getGalleriesAndTasks()
          }
        },
        controller: 'GalleryCtrl as vm',
        templateUrl: 'app/components/gallery/gallery.html'
      })
      .state('MultiChoice', {
        url: '/galleries/:galleryId/tasks/:taskId/multichoice',
        resolve: {
          task: function (TaskService, $stateParams) {
            return TaskService.retrieveTask($stateParams.galleryId, $stateParams.taskId);
          },
          gallery: function(GalleryService, $stateParams){
            return GalleryService.retrieveGallery($stateParams.galleryId);
          }
        },
        controller: 'MultipleChoiceTaskCtrl as vm',
        templateUrl: 'app/components/task/multichoice/task.html'
      })
      .state('Proximity', {
        url: '/galleries/:galleryId/tasks/:taskId/proximity',
        resolve: {
          task: function (TaskService, $stateParams) {
            return TaskService.retrieveTask($stateParams.galleryId, $stateParams.taskId);
          },
          gallery: function(GalleryService, $stateParams){
            return GalleryService.retrieveGallery($stateParams.galleryId);
          }
        },
        controller: 'FindTaskCtrl as vm',
        templateUrl: 'app/components/task/proximity/task.html'
      })
      .state('Photo', {
        url: '/galleries/:galleryId/tasks/:taskId/photo',
        resolve: {
          task: function (TaskService, $stateParams) {
            return TaskService.retrieveTask($stateParams.galleryId, $stateParams.taskId);
          },
          gallery: function(GalleryService, $stateParams){
            return GalleryService.retrieveGallery($stateParams.galleryId);
          }
        },
        controller: 'PictureTaskCtrl as vm',
        templateUrl: 'app/components/task/photo/task.html'
      })
      .state('PhotoManipulation', {
        cache: false,
        url: '/galleries/:galleryId/tasks/:taskId/photoManipulation',
        resolve: {
          task: function (TaskService, $stateParams) {
            return TaskService.retrieveTask($stateParams.galleryId, $stateParams.taskId);
          },
          gallery: function(GalleryService, $stateParams){
            return GalleryService.retrieveGallery($stateParams.galleryId);
          }

        },
        controller: 'PictureTaskManipulationCtrl as vm',
        templateUrl: 'app/components/task/photo/manipulation/manipulation.html'
      })
      .state('task-complete', {
        url: '/galleries/:galleryId/tasks/:taskId/complete',
        templateUrl: 'app/components/task/complete.html',
        controller: 'TaskCompleteCtrl as vm',
        resolve : {
          task: function(TaskService, $stateParams) {
            return TaskService.retrieveTask($stateParams.galleryId, $stateParams.taskId);
          },
          gameProgress: function(GameService){
            return GameService.getProgress();
          }
        }
      })
      .state('game-progress', {
        url:'progress',
        cache: false,
        templateUrl:'app/components/progress/progress.html',
        controller: 'ProgressCtrl as vm',
        resolve: {
          gameProgress: function(GameService){
            return GameService.getProgress();
          }
        }
      });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/home/welcome');

});