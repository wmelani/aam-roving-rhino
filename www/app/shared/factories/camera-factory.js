(function () {
    'use strict';
    angular.module('aam-roving-rhino').factory('Camera', ["$q", Camera]);

    function Camera($q) {
        function takePicture() {
            var q = $q.defer();
            
            if (!navigator.device || !navigator.device.capture) {
				var mockImageData = "http://placehold.it/1024x1024";
				q.resolve(mockImageData);
                return q.promise;
			}
            
            var options = {
                quality: 100,
                destinationType: navigator.camera.DestinationType.FILE_URI,
                sourceType: navigator.camera.PictureSourceType.CAMERA,
                allowEdit: false,
                encodingType: navigator.camera.EncodingType.JPEG,
                correctOrientation: true,
                saveToPhotoAlbum: false
            };

            navigator.camera.getPicture(
                function (imageData) {
                    q.resolve(imageData);
                },
                function (err) {
                    q.reject(err);
                },
                options);

            return q.promise;
        }

        function savePicture(imageData) {
            var q = $q.defer();

            if (imageData) {
                cordova.exec(
                    function (msg) {
                        q.resolve(msg);
                    },
                    function (err) {
                        q.reject(err);
                    },
                    "Canvas2ImagePlugin",
                    "saveImageDataToLibrary",
                    [imageData]);
            }
            else {
                console.log("Would have saved image but there was no image data.");
                q.resolve();
            }
            return q.promise;
        }

        return { takePicture: takePicture, savePicture: savePicture };
    }
})();