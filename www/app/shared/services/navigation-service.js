(function () {
    'use strict';
    angular.module('aam-roving-rhino').service('NavigationService', ['$localstorage', '$state', 'ConfigService', NavigationService]);

    var galleriesState = 'galleries',
        homeState = 'home',
        welcomeState = 'home.welcome',
        victoryState = 'home.victory',
        multiChoiceState = 'MultiChoice',
        proximityState = 'Proximity',
        photoState = 'Photo',
        manipulationState = 'PhotoManipulation',
        taskCompleteState = 'task-complete',
        progressState = 'game-progress';

    function NavigationService($localstorage,$state, ConfigService) {

        function canGoBack(){
            console.log("not implemented");
        }
        function goBack(){
            console.log("not implemented");
        }
        function goToGallery(){
            __go(galleriesState);
        }
        function goToComplete(galleryId,taskId){
            __go(taskCompleteState, {galleryId: galleryId, taskId: taskId});
        }
        function goToPhoto(galleryId, taskId){
            __go(photoState, {galleryId: galleryId, taskId: taskId});
        }
        function goToPhotoManipulation(galleryId,taskId,imageData){
            ConfigService.getConfig().then(function(conf){
                $localstorage.setObject(conf.imageKey,imageData);
                __go(manipulationState, {galleryId: galleryId, taskId: taskId});
            });
        }
        function goToHome(){
            __go(homeState);
        }
        function goToVictory(){
            __go(victoryState);
        }
        function goToMultiChoice(galleryId, taskId){
            __go(multiChoiceState, {galleryId: galleryId, taskId: taskId});
        }
        function goToProximity(galleryId, taskId){
            __go(proximityState, {galleryId: galleryId, taskId: taskId});
        }
        function goToWelcome(){
            __go(welcomeState);
        }
        function goToTask(galleryId,task){
            __go(task.Type, {galleryId: galleryId, taskId: task.Title});
        }
        function goToProgress(){
            __go(progressState);
        }
        function __go(name,obj){
            $state.go(name,obj);
        }

        return {
            canGoBack: canGoBack,
            goBack : goBack,
            goToGallery : goToGallery,
            goToComplete : goToComplete,
            goToPhoto : goToPhoto,
            goToTask : goToTask,
            goToPhotoManipulation : goToPhotoManipulation,
            goToHome : goToHome,
            goToVictory : goToVictory,
            goToMultiChoice : goToMultiChoice,
            goToProximity : goToProximity,
            goToWelcome : goToWelcome,
            goToProgress : goToProgress
        }
    }

})();