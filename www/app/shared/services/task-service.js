(function () {
    'use strict';
    angular.module('aam-roving-rhino').service('TaskService', ["$q", TaskService]);

    function TaskService($q) {
        function getTasks(){
            return $q(function(resolve, reject) {
                require(['app/config/Tasks'],function(data){
                    resolve(data);
                });
            });
        }

        function retrieveTask(galleryId, taskId){
            return $q(function(resolve, reject) {
                require(['app/config/Tasks'],function(tasks){
                    tasks.forEach(function(task){
                        if(task.Gallery === galleryId && task.Title === taskId) {
                            resolve(task);
                        }
                    });
                });
            });
        }

        return { getTasks : getTasks, retrieveTask : retrieveTask };
    }

})();