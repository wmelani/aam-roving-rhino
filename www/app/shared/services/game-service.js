(function () {
    'use strict';

	var titles = [
		"EXPLORER", "SENSEI", "GURU"
	];

	function GameProgress(startingPoints, tasks) {
		this.points = startingPoints;

		var totalPoints = 0;
		tasks.forEach(function (task) {
			totalPoints += task.Points;
		});
		this.totalPoints = totalPoints;
	}

	GameProgress.prototype.updateProgress = function (points) {
		var startingPercent = this.getPercentComplete();
		this.points = Number(this.points) + Number(points);
		
		//if we passed a 20 percent mark then we want to show progress
		if(this.getPercentComplete() % 20 < startingPercent % 20){
			this.showProgress = true;
		}
	};
	
	GameProgress.prototype.getTitle = function () {
		if ((this.points * 4) < this.totalPoints) {
			return titles[0];
		}
		else if ((this.points * 2) < this.totalPoints) {
			return titles[1];
		}
		else {
			return titles[2];
		}
	};
	GameProgress.prototype.getPercentComplete = function () {
		return (this.points / this.totalPoints) * 100;
	};
	GameProgress.prototype.getPoints = function () {
		return this.points;
	};
	GameProgress.prototype.getNextTitle = function(){
		var currentTitleIndex = titles.indexOf(this.getTitle());
		return titles[currentTitleIndex+1];
	};
	GameProgress.prototype.checkShowProgress = function(){
		if(this.showProgress){
			//Reset the showProgress flag and return true.
			this.showProgress = false;
			return true;
		}
		return false;
	};


    angular.module('aam-roving-rhino').service('GameService', ['$localstorage', 'TaskService', '$q', GameService]);

    function GameService($localstorage, TaskService, $q) {
		var gameProgress;

		var initGameProgress = function (tasks) {
			gameProgress = new GameProgress(getPoints(), tasks);
		};

        this.completeTask = function (task) {
            var tasks = $localstorage.getObject("tasks");
			var visited = tasks[task.Title];
			if (!visited) {
				gameProgress.updateProgress(task.Points);

				var points = parseInt($localstorage.get("points")) + parseInt(task.Points);
				$localstorage.set("points", points);
				tasks[task.Title] = true;
				console.log("tasks: " + JSON.stringify(tasks));
				$localstorage.setObject("tasks", tasks);
			}
        };

		this.continueGame = function () {
			var storedTasks = $localstorage.getObject('tasks');
			var taskPromise = TaskService.getTasks();

			taskPromise.then(function (tasks) {
				tasks.forEach(function (task) {
					if (storedTasks[task.Title]) {
						task.IsComplete = true;
					}
				});
			});
			taskPromise.then(function (tasks) {
				initGameProgress(tasks);
			});
		};

		this.initializeGame = function () {
			$localstorage.clear();
			$localstorage.set("points", "0");
			$localstorage.setObject("tasks", {});

			TaskService.getTasks().then(function (tasks) {
				initGameProgress(tasks);
			});

		};

		var getPoints = function () {
			if (!$localstorage.get("points")) {
				$localstorage.set("points", "0");
				$localstorage.setObject("tasks", {});
			}

			return $localstorage.get("points");
		};
		
		this.getPoints = getPoints;

		this.getProgress = function(){
			return $q(function(resolve, reject){
				if (gameProgress) {
					resolve(gameProgress);
				}
				else {
					TaskService.getTasks().then(function(tasks){
						initGameProgress(tasks);
						resolve(gameProgress);
					});
				}
			});
		};

	}

})();