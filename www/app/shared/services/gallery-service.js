(function () {
    'use strict';
    angular.module('aam-roving-rhino').service('GalleryService', ["$q", "TaskService", GalleryService]);

    function GalleryService($q, TaskService) {
        function getGalleries() {
            return $q(function (resolve, reject) {
                require(['app/config/Galleries'], function (data) {
                    resolve(data);
                });
            });
        }

        function retrieveGallery(galleryId) {
            var galleriesPromise = this.getGalleries();
            return $q(function (resolve, reject) {
                galleriesPromise.then(function (galleries) {
                    galleries.forEach(function (gallery) {
                        if (gallery.Name === galleryId) {
                            resolve(gallery);
                        }
                    });
                });
            });
        }

        function getGalleriesAndTasks() {
            var galleriesPromise = this.getGalleries();
            var tasksPromise = TaskService.getTasks();

            var galleriesAndTasks = $q(function (resolve, reject) {
                $q.all([galleriesPromise, tasksPromise]).then(function (data) {
                    var galleries = data[0];
                    var tasks = data[1];

                    for (var g = 0; g < galleries.length; g++) {
                        var gallery = galleries[g];
                        gallery.tasks = [];
                        for (var t = 0; t < tasks.length; t++) {
                            var task = tasks[t];
                            if (task.Gallery == gallery.Name) {
                                gallery.tasks.push(task);
                            }
                        }
                    }

                    resolve(galleries);
                });
            });
            return galleriesAndTasks;
        }
        return {
            getGalleries: getGalleries,
            getGalleriesAndTasks: getGalleriesAndTasks,
            retrieveGallery: retrieveGallery
        };
    }
})();


		