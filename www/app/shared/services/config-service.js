(function () {
    'use strict';
    angular.module('aam-roving-rhino').service('ConfigService', ["$q", ConfigService]);

    function ConfigService($q) {
        function getConfig(){
            return $q(function(resolve, reject) {
                require(['app/config/app.config'],function(data){
                    resolve(data);
                });
            });
        }
        return { getConfig : getConfig };
    }

})();