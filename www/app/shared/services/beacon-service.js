(function () {
  'use strict';
  angular.module('aam-roving-rhino').service('BeaconService', ["$q", "$window", "$localstorage", "$rootScope", BeaconService]);

  function BeaconService($q, $window, $localstorage, $rootScope) {

    var beaconExpirationThreshold = 60000;

    function initialize() {
      if (isCordova()) {
        var delegate = createDelegate();

        // start collecting data
        //$window.cordova.plugins.locationManager.requestAlwaysAuthorization();
        $window.cordova.plugins.locationManager.requestWhenInUseAuthorization();
        $window.cordova.plugins.locationManager.setDelegate(delegate);
      }
    }

    function createDelegate() {
      var delegate = new cordova.plugins.locationManager.Delegate();

      delegate.didRangeBeaconsInRegion = function(pluginResult) {

        console.log('didRangeBeaconsInRegion()', JSON.stringify(pluginResult));

        pluginResult.beacons.forEach(function(result) {
          var foundBeacon = parseBeaconResult(result);

          $localstorage.setObject(foundBeacon.minor, foundBeacon);
          
          $rootScope.$broadcast("updated ranging event", foundBeacon.minor);
        });
      }

      return delegate;
    }

    function parseBeaconResult(result) {
      return {
        uuid: result.uuid,
        major: result.major,
        minor: result.minor,
        rssi: result.rssi,
        proximity: result.proximity,
        accuracy: result.accuracy,
        timestamp: new Date()
      }
    }

    // tell the service to start watching the proximities to beacons
    function startRanging(region) {
      console.log('startRanging()');

      var beaconRegion = cordova.plugins.locationManager.Regions.fromJson(region);

      console.log('Parsed BeaconRegion object:', JSON.stringify(beaconRegion, null, '\t'));

      $window.cordova.plugins.locationManager.startRangingBeaconsInRegion(beaconRegion);
    }

    function enableBluetooth() {
      if (ionic.Platform.isAndroid()) {
        alert('Enabling Bluetooth.');
        cordova.plugins.locationManager.enableBluetooth();
      }
      else {
        alert('Please enable Bluetooth in your device settings to continue.');
      }
    }

    function checkBluetooth() {
      var promise;
      if (isCordova()) {
        promise = cordova.plugins.locationManager.isBluetoothEnabled();
      }
      else {
        promise = $q.when(false);
      }
      return promise;
    }

    function isRecentBeacon(beaconTime) {
			return (new Date() - Date.parse(beaconTime) < beaconExpirationThreshold);
    }

    function isCordova() {
      return $window.cordova !== undefined;
    }

    return {
      initialize: initialize,
      startRanging: startRanging,
      enableBluetooth: enableBluetooth,
      checkBluetooth: checkBluetooth,
      isCordova: isCordova,
      isRecentBeacon: isRecentBeacon};
    }
  })();
