angular.module('aam-roving-rhino')
.service('SocialMediaService', ['$cordovaSocialSharing', function($cordovaSocialSharing){
	this.shareAnywhere = function(post) {
		console.log("Sharing: ", JSON.stringify(post));
		$cordovaSocialSharing.share(post.message, post.subject, post.image, post.link)
			.then(function (result) {
				console.log("shared: ", JSON.stringify(result));
			}, 
			function (err) {
				console.log("error sharing", err);
			});
	};
	
	this.shareToInstagram = function(post) {
		console.log("Sharing to instagram... ", JSON.stringify(post));
		$cordovaInstagram.share(post.image, post.message).then(function() {
			console.log("Shared to instagram: ", JSON.stringify(post));
		}, function (err) {
			console.log("error sharing to instagram", err);
		});
	};
	
}]);