(function () {
    'use strict';
    angular.module('aam-roving-rhino')
		.controller('GameFooterCtrl', ['GameService', function (GameService) {
			var vm = this;
			
			GameService.getProgress().then(function(gameProgress){
				vm.gameProgress = gameProgress;
			});
		}])
		.directive('gameFooter', function () {
			return {
				restrict: 'E',
				templateUrl: 'app/shared/views/footer/footer.html'
			};
		});
})();