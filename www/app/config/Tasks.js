﻿define (function() {
	var uuid = 'F185FA2C-E7E4-4B81-B536-7F6CD0F08EB9';
	
	//In case we ever want to give a different major to the different galleries.
	var southAsiaMajor = 100,
	persiaMajor = 100,
	southeastAsiaMajor = 100,
	himalayasMajor = 100,
	chinaMajor = 100,
	koreaMajor = 100,
	japanMajor = 100,
	otherMajor = 100;
	
	return [
		{
			"Title": "Donate to Ganesh",
			"Description": "Upload a picture of yourself donating to Ganesh.",
			"Points": 10,
			"Type": "Photo",
			"Gallery": "South Asia",
			"FrameFilename": "img/frames/kingkong.png"
		},
		{
			"Title": "Strike a Pose with Buddha",
			"Description": "Upload a picture of you posing like Buddha.",
			"Points": 10,
			"Type": "Photo",
			"Gallery": "China",
			"FrameFilename": "img/frames/kingkong.png"
		},
		{
			"Title": "Count the Knives",
			"Description": "How many knives are in the cabinet?",
			"Points": 30,
			"Type": "MultiChoice",
			"Gallery": "Southeast Asia",
			"MultiChoice": {
				"Question": "How many knives are in the knife cabinet?",
				"PossibleAnswers": [
					{
						"Answer": "7",
						"IsCorrect": false,
						"TryAgainText": "hint text here "
					},
					{
						"Answer": "9",
						"IsCorrect": false
					},
					{
						"Answer": "14",
						"IsCorrect": false
					},
					{
						"Answer": "19",
						"IsCorrect": true
					},
					{
						"Answer": "23",
						"IsCorrect": false
					}
				]
			}
		},
		{
			"Title": "Find the Oldest Artifact",
			"Description": "Where is the oldest artifact?",
			"Points": 20,
			"Type": "Proximity",
			"Gallery": "The Persian World and West Asia",
			"BeaconUUID": uuid,
			"BeaconMajor": persiaMajor,
			"BeaconMinor": 0,
			"BeaconIdentifier": "Find the Oldest Artifact Beacon"
		},
		{
			"Title": "The Bone Collector",
			"Description": "Can you find the jar that once contained human bones?",
			"Points": 20,
			"Type": "Proximity",
			"Gallery": "Southeast Asia",
			"BeaconUUID": uuid,
			"BeaconMajor": southAsiaMajor,
			"BeaconMinor": 1,
			"BeaconIdentifier": "The Bone Collector",
			"SuccessMessage": "Congratulations! A tradition of jar burials existed in the Philippines since ancient times and continues to the present day."
		},
		{
			"Title": "Find the Pillows",
			"Description": "Find the objects in Samsung Hall that are used as pillows.",
			"Points": 20,
			"Type": "Proximity",
			"Gallery": "Unknown"
		},
		{
			"Title": "Still Water",
			"Description": "Can you find the sculpture that contains water?",
			"Points": 20,
			"Type": "Proximity",
			"Gallery": "Japan",
			"BeaconUUID": uuid,
			"BeaconMajor": japanMajor,
			"BeaconMinor": 3,
			"BeaconIdentifier": "Still Water",
			"SuccessMessage": "Sit and count your breath for 20 breaths."
		},
		{
			"Title": "Overlooked Overseer",
			"Description": "Can you find the protective monster that guards the room? (Hint: Look up!)",
			"Points": 20,
			"Type": "Proximity",
			"Gallery": "Southeast Asia",
			"BeaconUUID": uuid,
			"BeaconMajor": southeastAsiaMajor,
			"BeaconMinor": 4,
			"BeaconIdentifier": "Overlooked Overseer",
			"SuccessMessage": "Congratulations! Mythical serpents such as this were used as roof ornaments and at the ends of stair handrails to protect those under their watch."
		},
		{
			"Title": "Shiva, Parvati & Skanda statue - what are the rings for?",
			"Description": "This stone sculpture shows Shiva, Parvati and little Skanda in traditional poses.",
			"Points": 30,
			"Type": "MultiChoice",
			"Gallery": "Southeast Asia",
			"Beacon": null,
			"MultiChoice": {
				"Question": "What are the rings used for?",
				"PossibleAnswers": [
					{
						"Answer": "To prevent theft from the temple",
						"IsCorrect": false
					},
					{
						"Answer": "To carry the sculpture in festivals",
						"IsCorrect": true
					},
					{
						"Answer": "To tether horses",
						"IsCorrect": false
					},
					{
						"Answer": "To hang garlands from",
						"IsCorrect": false
					}
				]
			}
		},
		{
			"Title": "Wall 'O Monkeys",
			"Description": "How many monkeys are attacking Kumbhakarna? Count the monkey heads.",
			"Points": 30,
			"Type": "MultiChoice",
			"Gallery": "Southeast Asia",
			"MultiChoice": {
				"Question": "How many monkeys are attacking Kumbhakarna? Count the monkey heads.",
				"PossibleAnswers": [
					{
						"Answer": "5",
						"IsCorrect": false
					},
					{
						"Answer": "8",
						"IsCorrect": false
					},
					{
						"Answer": "16",
						"IsCorrect": true
					},
					{
						"Answer": "32",
						"IsCorrect": false
					}
				]
			}
		},
		{
			"Title": "Hidden History",
			"Description": "What was this building before it was the Asian Art Museum?",
			"Points": 30,
			"Type": "MultiChoice",
			"Gallery": "Unknown",
			"MultiChoice": {
				"Question": "What was this building before it was the Asian Art Museum?",
				"PossibleAnswers": [
					{
						"Answer": "City Hall",
						"IsCorrect": false
					},
					{
						"Answer": "Library",
						"IsCorrect": true
					},
					{
						"Answer": "School",
						"IsCorrect": false
					},
					{
						"Answer": "De Young Museum",
						"IsCorrect": false
					}
				]
			}
		},
		{
			"Title": "Become a Samurai",
			"Description": "Take a Picture of yourself wearing samurai armor. #AAMscavengerhunt",
			"Points": 10,
			"Type": "Photo",
			"Gallery": "Japan",
			"FrameFilename": "samurai.png"
		},
		{
			"Title": "A Horse in the Room",
			"Description": "How many horses are in this room?",
			"Points": 30,
			"Type": "MultiChoice",
			"Gallery": "China",
			"Beacon": null,
			"MultiChoice": {
				"Question": "How many horses are in this room?",
				"PossibleAnswers": [
					{
						"Answer": "1",
						"IsCorrect": false
					},
					{
						"Answer": "6",
						"IsCorrect": true
					},
					{
						"Answer": "18",
						"IsCorrect": false
					},
					{
						"Answer": "32",
						"IsCorrect": false
					}
				]
			}
		},
		{
			"Title": "Royal Ease",
			"Description": "Find the most comfortable Buddha in this gallery. Take a picture of yourself in the same pose. #AAMscavengerhunt",
			"Points": 10,
			"Type": "Photo",
			"Gallery": "China",
			"FrameFilename": "img/frames/kingkong.png",
			"SuccessMessage": "Now that you're rested, take the stairs or elevator to the second floor."
		},
		{
			"Title": "Ganesh's Tusk",
			"Description": "How did Ganesh lose his tusk?",
			"Points": 30,
			"Type": "MultiChoice",
			"Gallery": "Southeast Asia",
			"MultiChoice": {
				"Question": "How did Ganesh lose his tusk?",
				"PossibleAnswers": [
					{
						"Answer": "He lost it fighting with Kartikeya (his brother)",
						"IsCorrect": false
					},
					{
						"Answer": "He broke one tusk off because he needed something to write with",
						"IsCorrect": true
					},
					{
						"Answer": "He broke it off to dig a trench for water",
						"IsCorrect": false
					}
				]
			}
		},
		{
			"Title": "Buddha",
			"Description": "Who was Buddha?",
			"Points": 30,
			"Type": "MultiChoice",
			"Gallery": "Southeast Asia",
			"MultiChoice": {
				"Question": "Who was Buddha?",
				"PossibleAnswers": [
					{
						"Answer": "A warrior from China",
						"IsCorrect": false
					},
					{
						"Answer": "A rotund man from Persia",
						"IsCorrect": false
					},
					{
						"Answer": "A prince from the foothills of the Himalyas",
						"IsCorrect": true
					},
					{
						"Answer": "A fisherman from Japan",
						"IsCorrect": false
					}
				]
			}
		},
		{
			"Title": "Elephant Head",
			"Description": "​How did Ganesha get his elephant head?",
			"Points": 30,
			"Type": "MultiChoice",
			"Gallery": "South Asia",
			"MultiChoice": {
				"Question": "​How did Ganesha get his elephant head?",
				"PossibleAnswers": [
					{
						"Answer": "He found it under his bed.",
						"IsCorrect": false
					},
					{
						"Answer": "He was born that way.",
						"IsCorrect": false
					},
					{
						"Answer": "It was a gift from a friend.",
						"IsCorrect": false
					},
					{
						"Answer": "His father gave it to him.",
						"IsCorrect": true
					}
				]
			}
		},
		{
			"Title": "What is Asia",
			"Description": "Which country is not in Asia?",
			"Points": 30,
			"Type": "MultiChoice",
			"Gallery": "South Asia",
			"MultiChoice": {
				"Question": "Which country is not in Asia?",
				"PossibleAnswers": [
					{
						"Answer": "Indonesia",
						"IsCorrect": false
					},
					{
						"Answer": "Mongolia",
						"IsCorrect": false
					},
					{
						"Answer": "Egypt",
						"IsCorrect": true
					},
					{
						"Answer": "Sri Lanka",
						"IsCorrect": false
					}
				]
			}
		},
		{
			"Title": "Centaur (Half-man/Half horse)",
			"Description": "Can you find the figure wearing a centaur (half-man/half-horse) necklace?",
			"Points": 20,
			"Type": "Proximity",
			"Gallery": "South Asia",
			"BeaconUUID": uuid,
			"BeaconMajor": southAsiaMajor,
			"BeaconMinor": 5,
			"BeaconIdentifier": "Centaur",
			"SuccessMessage": "Congratulations! You've found a divine Buddhist being (bodhisattava).",
			"Ignore": true
		},
		{
			"Title": "Enlightened Buddha",
			"Description": "Can you find the Buddha flanked by lions and touching the earth?",
			"Points": 20,
			"Type": "Proximity",
			"Gallery": "South Asia",
			"BeaconUUID": uuid,
			"BeaconMajor": southAsiaMajor,
			"BeaconMinor": 6,
			"BeaconIdentifier": "Enlightened Buddha",
			"SuccessMessage": "Congratulations! You've found the Buddha triumphing over the demon Mara. The Buddha achieved enlightenment when meditating under the bodhi tree--even after the demon Mara challenged him."
		},
		{
			"Title": "The Dancing Child",
			"Description": "Can you find the dancing child?",
			"Points": 20,
			"Type": "Proximity",
			"Gallery": "South Asia",
			"BeaconUUID": uuid,
			"BeaconMajor": southAsiaMajor,
			"BeaconIdentifier": "The Dancing Child",
			"BeaconMinor": 7
		},
		{
			"Title": "Riding in Style",
			"Description": "Find the artwork that will allow you to ride an elephant in style.",
			"Points": 20,
			"Type": "Proximity",
			"Gallery": "South Asia",
			"BeaconUUID": uuid,
			"BeaconMajor": southAsiaMajor,
			"BeaconMinor": 8,
			"BeaconIdentifier": "Riding in Style",
			"SuccessMessage": "Congratulations! You've found the elephant throne (howdah). Count the number of animals on this throne (look for birds, lions, snakes, monkeys, and bulls). Good luck..."
		},
		{
			"Title": "A Holy Book",
			"Description": "Can you find Islam's holiest book?",
			"Points": 20,
			"Type": "Proximity",
			"Gallery": "The Persian World and West Asia",
			"BeaconUUID": uuid,
			"BeaconMajor": persiaMajor,
			"BeaconMinor": 9,
			"BeaconIdentifier": "A Holy Book",
			"SuccessMessage": "Congratulations! You've found the Qu'ran, the holiest book in Islam."
		},
		{
			"Title": "Wall of Daggers",
			"Description": "What materials were not used to make these daggers?",
			"Points": 30,
			"Type": "MultiChoice",
			"Gallery": "Southeast Asia",
			"MultiChoice": {
				"Question": "What materials were not used to make these daggers?",
				"PossibleAnswers": [
					{
						"Answer": "Bones",
						"IsCorrect": false
					},
					{
						"Answer": "Steel",
						"IsCorrect": false
					},
					{
						"Answer": "Aluminum",
						"IsCorrect": true
					},
					{
						"Answer": "Whale Tooth",
						"IsCorrect": false
					}
				]
			}
		},
		{
			"Title": "Indonesian Rod Puppets",
			"Description": "Pick your favorite puppet.What word best describes their face?",
			"Points": 30,
			"Type": "MultiChoice",
			"Gallery": "Southeast Asia",
			"MultiChoice": {
				"Question": "Pick your favorite puppet. What word best describes their face?",
				"PossibleAnswers": [
					{
						"Answer": "Angry",
						"IsCorrect": true
					},
					{
						"Answer": "Funny",
						"IsCorrect": true
					},
					{
						"Answer": "Scary",
						"IsCorrect": true
					},
					{
						"Answer": "Pretty",
						"IsCorrect": true
					}
				]
			}
		},
		{
			"Title": "Beautiful Stones",
			"Description": "Jade is admired for many reasons. Which of these is not correct?",
			"Points": 30,
			"Type": "MultiChoice",
			"Gallery": "Jade Gallery",
			"MultiChoice": {
				"Question": "Jade is admired for many reasons. Which of these is not correct?",
				"PossibleAnswers": [
					{
						"Answer": "Brillance",
						"IsCorrect": false
					},
					{
						"Answer": "Malleability",
						"IsCorrect": true
					},
					{
						"Answer": "Hardness",
						"IsCorrect": false
					},
					{
						"Answer": "Transparency",
						"IsCorrect": false
					}
				]
			}
		},
		{
			"Title": "Strange Clothing",
			"Description": "What is Simhavaktra Dakini's cape made from?",
			"Points": 30,
			"Type": "MultiChoice",
			"Gallery": "The Himalayas and the Tibetan Buddhist World",
			"Ignore": true,
			"MultiChoice": {
				"Question": "What is Simhavaktra Dakini's cape made from?",
				"PossibleAnswers": [
					{
						"Answer": "Silk",
						"IsCorrect": false
					},
					{
						"Answer": "Cotton",
						"IsCorrect": false
					},
					{
						"Answer": "Tiger Skin",
						"IsCorrect": false,
						"TryAgainText": "Close, but what is she wearing around her shoulders?"
					},
					{
						"Answer": "Human Skin",
						"IsCorrect": true
					}
				]
			}
		},
		{
			"Title": "Inside the Rhino",
			"Description": "What is inside this rhino?",
			"Points": 30,
			"Type": "MultiChoice",
			"Gallery": "China",
			"Beacon": null,
			"Ignore": true,
			"MultiChoice": {
				"Question": "What is inside this rhino?",
				"PossibleAnswers": [
					{
						"Answer": "Writing",
						"IsCorrect": true
					},
					{
						"Answer": "Wine",
						"IsCorrect": false
					},
					{
						"Answer": "Ashes",
						"IsCorrect": false
					},
					{
						"Answer": "Chocolate",
						"IsCorrect": false
					}
				]
			}
		},
		{
			"Title": "Wall of Symbols",
			"Description": "What does the bat symbolize?",
			"Points": 30,
			"Type": "MultiChoice",
			"Gallery": "China",
			"Beacon": null,
			"MultiChoice": {
				"Question": "What does the bat symbolize?",
				"PossibleAnswers": [
					{
						"Answer": "Death",
						"IsCorrect": false
					},
					{
						"Answer": "Blessings",
						"IsCorrect": true
					},
					{
						"Answer": "Love",
						"IsCorrect": false
					},
					{
						"Answer": "Vampires",
						"IsCorrect": false
					}
				]
			}
		},
		{
			"Title": "Smoking a Pipe",
			"Description": "Can you find the animal smoking a pipe?",
			"Points": 20,
			"Type": "Proximity",
			"Gallery": "Korea",
			"BeaconUUID": "B9407F30-F5F8-466E-AFF9-25556B57FE6D",
			"BeaconMajor": 58150,
			"BeaconMinor": 7118,
			"BeaconIdentifier": "Smoking a Pipe",
			"SuccessMessage": "Congratulations! You've found the tiger smoking a pipe."
		},
		{
			"Title": "Colorful Cloths",
			"Description": "How would these colorful cloths (bojagi) not have been used?",
			"Points": 30,
			"Type": "MultiChoice",
			"Gallery": "Korea",
			"Beacon": null,
			"MultiChoice": {
				"Question": "How would these colorful cloths (bojagi) not have been used?",
				"PossibleAnswers": [
					{
						"Answer": "Table Cover",
						"IsCorrect": false
					},
					{
						"Answer": "Gift Wrap",
						"IsCorrect": false
					},
					{
						"Answer": "Face Tissue",
						"IsCorrect": true
					},
					{
						"Answer": "Decoration",
						"IsCorrect": false
					}
				]
			}
		},
		{
			"Title": "Tattoos",
			"Description": "Can you find the objects with tattoos on their faces?",
			"Points": 20,
			"Type": "Proximity",
			"Gallery": "Japan",
			"BeaconUUID": uuid,
			"BeaconMajor": japanMajor,
			"BeaconMinor": 2,
			"BeaconIdentifier": "Tattoos",
			"SuccessMessage": "Congratulations! You've found the haniwa. These figures were placed on burial mounds in ancient Japan."
		}];
});
