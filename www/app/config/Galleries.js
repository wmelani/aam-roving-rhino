﻿define(function() { return [
	{ 
		"Name": "South Asia",
		"ShortName": "south-asia"
	},
	{
		"Name": "The Persian World and West Asia",
		"ShortName": "persia",
	},
	{
		"Name": "Southeast Asia",
		"ShortName": "southeast-asia",
	},
	{
		"Name": "The Himalayas and the Tibetan Buddhist World",
		"ShortName": "himalayas"
	},
	{
		"Name": "China",
		"ShortName": "china"
	},
	{
		"Name": "Korea",
		"ShortName":"korea"
	},
	{
		"Name": "Japan",
		"ShortName":"japan"
	}
]});
