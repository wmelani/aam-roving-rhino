#DO YOU LIKE RHINOS?#

##Deploying

These are the steps I follow to get this thing deployed onto my phone successfully. Deploying with Ionic is somewhat of a pain because it will get installed on your device and then you'll try to use the camera feature and you'll realize you have missing plugins. Because of this, when you finally get it installed, make sure you run through all of the features in the app (taking a picture, social media sharing, BLE, etc).

The instructions below are not helpful in trying to get set up with iOS developer profiles and all that stuff that you need. These are for when you've figured out how to get an app on your phone but for sonme reason it's still not working 100% correctly because it is missing plugins.

Just a disclaimer: some of these steps are probably not necessary.

1. Remove the platform you are trying to add `ionic platform remove ios`
2. Delete the plugins folder entirely `rm -rf plugins` (If you're on Windows, follow [these steps](https://msysgit.github.io/) to install a bash shell so you can get some real work done).
3. Go ahead and delete the platform folder too while you're at it, what can go wrong? `rm -rf platforms`
4. Add the platform back `ionic platform add ios`
5. Remove the platform one more time, for good measure `ionic platform remove ios`
6. Go ahead and add that platform yet again `ionic platform add ios`
7. Deploy to your phone `ionic run ios --device [-c -l]` (You can try using -c and -l to get context logging and live-reload, although live-reload increases your chances for weirdness).
8. Test the app and make sure all of the features work (this ensures that all the necessary plugins were added).
